import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, map, shareReplay, takeUntil } from 'rxjs/operators';
import { CommonService } from 'src/app/shared/services/common.service';
import { storePhoto } from 'src/app/store/actions/photo.actions';
import { AppState } from 'src/app/store/reducers';
import { Photos } from '../../models/photos';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-photos-list',
  templateUrl: './photos-list.component.html',
  styleUrls: ['./photos-list.component.scss']
})
export class PhotosListComponent implements OnInit, OnDestroy {
  public photos$: Observable<Photos[]>;
  destroy$: Subject<Photos[]> = new Subject<Photos[]>();
  public errorMsg: string;

  constructor(
    private dashboardService: DashboardService,
    private store: Store<AppState>,
    private commonService: CommonService) { }

  ngOnInit(): void {
    this.photos$ = this.dashboardService
      .getAllPhotos()
      .pipe(
        catchError(error => {
          if (error.error instanceof ErrorEvent) {
            this.errorMsg = `Error: ${error.error.message}`;
          } else {
            this.errorMsg = this.commonService.getServerErrorMessage(error);
          }
          return throwError(this.errorMsg);
        }),
        map(photos => photos),
        takeUntil(this.destroy$),
        shareReplay()
      );
  }

  ngOnDestroy() {
    this.destroy$.next(undefined);
    this.destroy$.unsubscribe();
  }

  removePhotos = (photo: Photos, photos: Photos[]) => {
    this.store.dispatch(storePhoto({ photos: photo }));
    const filteredData: Photos[] = photos.filter(pho => pho.id !== photo.id);
    if (filteredData && filteredData.length > 0) {
      this.photos$ = of(filteredData);
    }
  }
}
