import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of, pipe, Subject, throwError } from 'rxjs';
import { catchError, map, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { CommonService } from 'src/app/shared/services/common.service';
import { AppState } from 'src/app/store/reducers';
import { Posts } from '../../models/posts';
import { DashboardService } from '../../services/dashboard.service';
import { storePosts } from './../../../store/actions/posts.actions';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit, OnDestroy {

  public posts$: Observable<Posts[]>;
  destroy$: Subject<Posts[]> = new Subject<Posts[]>();
  public errorMsg: string;

  constructor(
    private dashboardService: DashboardService,
    private commonService: CommonService,
    private store: Store<AppState>) { }

  ngOnInit(): void {
    this.posts$ = this.dashboardService
      .getAllPosts()
      .pipe(
        catchError(error => {
          if (error.error instanceof ErrorEvent) {
            this.errorMsg = `Error: ${error.error.message}`;
          } else {
            this.errorMsg = this.commonService.getServerErrorMessage(error);
          }
          return throwError(this.errorMsg);
        }),
        map(posts => posts),
        takeUntil(this.destroy$),
        shareReplay()
      );
  }

  ngOnDestroy() {
    this.destroy$.next(undefined);
    this.destroy$.unsubscribe();
  }

  removePost = (post: Posts, posts: Posts[]) => {
    this.store.dispatch(storePosts({ posts: post }));
    const filteredData: Posts[] = posts.filter(pos => pos.id !== post.id);
    if (filteredData && filteredData.length > 0) {
      this.posts$ = of(filteredData);
    }
  }
}
