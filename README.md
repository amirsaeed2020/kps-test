# KPS FE Candidate TEST - Angular

## How to run the code on your local host

### Systme Requirements
You need the below requirements to run the application on your localhost
- Node 15.4.0
- Npm 6.14.11
- Angular Cli 11.2.11

### How to run the Application
The application is divided into two , server and client.

Navigate to the main root directory and 
Run the below command

> npm install

After all node packages are installed then naviage to the server directory.

To run the server,  Please navigate to the server directory through your termainl / command window and run 

> npm run server

It will run your server on port : 9000, 

paste the below url into your browser window to see whether your prduct API end point is working or not

> http://localhost:9000/api/products, 

if you seee the results,then your server is up and running. 

Open another terminal/command prompt window and navigate to the root directory of the application and run the below command

> npm run start

after few minutes, your application will be running on 

> http://localhost:4200, 

copy and paste this url into browser window and you will see
the application running. 

> There is movie i recorded to show the functionality of the whole application, you can view here
> https://bitbucket.org/amirsaeed2020/kps-test/src/master/KPS-Test-Screen.mov
> you can download the movie by clicking the raw url 

To run the linting for the project, use this below command
> npm run linting

To run the test, use this below command

> npm run test

<br />

## Requirements 

This is a test that will allow applying candidate to demonstrate their coding skills.

## Fork the repository

This repository is read only so to push your changes you'll need to fork it.

- In Bitbucket click the `+` button on the left hand navigation
- In the _GET TO WORK_ section, choose _Fork this repository_
- In your forked repo click the _Settings_ cog in the left hand navigation and choose _General_ / _User and group access_
  Give read access to `lukasz.pietraszek@uk.kps.com`, `melanie.berrut@uk.kps.com` and `marcin.palka@uk.kps.com`



## Keep in mind
- Please use the latest version of Angular when working on this task.
- Try to have a least one commit per task so that we can see the work progress
- Use descriptive commit comments
- It's up to you whether you want to push regularly or once you are finished with all of your commits
- Things to consider: performance, efficient use of space, usability, cross-browser, responsiveness.
- Show that you understand how version control works by using following commands: `branch`, `merge` and `rebase`


## Tasks

### 1. Create product listing page

- Create a route called `/products` where you will have a full width page with a header, a footer and a main section.
- The page should be fully responsive.
- Add toggle option between list and grid view
- The main section should be populated with products coming from an HTTP request to an endpoint containing the data from a `products.json`
- Each product should include
    - Image thumbnail
    - Product name
    - Product price (current, was and discount price),
    - Size
    - Link to `url` key
    - Add favourite
- Desktop design for products layout is provided in `<root>/plp-design.png`
- Add Unit Tests
- Add comments on what other improvements would you add to this page
- **Optional task:** The data set is reasonably large so we need to be able to: 
    - **filter the data by:** category, price, size
    - **sort the data by:** price (ascending), price (descending) and name (ascending)


### 2. Create dashboard

- Create a route called `/dashboard` where you will have a full width page with a header, a footer and a main section.
- Prepare three components:
    - The first component should display a list of posts from this endpoint https://jsonplaceholder.typicode.com/posts. There should be button "Remove" next to each item in the list
    - The second component should display a list of photos from this endpoint https://jsonplaceholder.typicode.com/photos. There should be button "Remove" next to each item in the list
    - The third component should be named "Removed Items". When the user clicks on the "Remove" buttons, the item should be removed from the list and should be presented in the "Removed Items" component. Please try to avoid using `@Input` and `@Output` decorators.
- Add Unit Tests


## Finished

Once you've completed your test please **make sure you shared repo access to the reviewers ("Fork the repository" section), to allow us to clone the repository locally and run it.**

Please email `stephanie.wilson@uk.kps.com` so that we know your work is ready to be reviewed.
